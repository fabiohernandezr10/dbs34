import psycopg2 

conn = psycopg2.connect(
    host = "localhost",
    database="transportes",
    user="postgres",
    password="admin")

cur = conn.cursor()
"""
#traernos la versión de postgres instalada
query = "SELECT version()" #escritura de la query
cur.execute(query) #ejecución de la query 
db_version=cur.fetchone() #para leer lo que nos retorna la query y almacenarlo en la variable db_version
print(db_version)
"""
"""
query = "SELECT \"nombre\",\"direccion\","+\
    "\"telefono\",\"fecha_nacimiento\""+\
        " from \"Pasajeros\";"#traer los registros de la tabla Pasajeros
cur.execute(query)
response= cur.fetchall()#para leer lo que nos retorna la query
print(response)
for reg in response:
    nombre=reg[0]
    dir=reg[1]
    tel=reg[2]
    fecha=reg[3]
    print("nombre: ",nombre," dir: ",dir," tel: ",tel," fecha: ",fecha)
"""
"""
try:
    query = "INSERT INTO \"Pasajeros\""+\
        "(nombre, direccion, telefono, fecha_nacimiento)"+\
            "VALUES ('Andres','C3 123123','(123) 1231231','2000-11-11')"
    cur.execute(query)
    conn.commit()
except(Exception, psycopg2.DatabaseError) as error:
        print(error)
"""
"""
try: 
    query = "DELETE FROM \"Pasajerossdfasdf\""+\
        "WHERE id=41"
    cur.execute(query)
    conn.commit()
    cur.close()
 
except(Exception, psycopg2.DatabaseError) as error:
        print(error)
"""

try: 
    query = "UPDATE \"Pasajeros\""+\
        "SET nombre='Antonio' WHERE id=40"
    cur.execute(query)
    conn.commit()
    cur.close()
 
except(Exception, psycopg2.DatabaseError) as error:
        print(error)